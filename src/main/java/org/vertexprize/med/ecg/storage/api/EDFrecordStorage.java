/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.med.ecg.storage.api;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;
import org.eclipse.serializer.persistence.types.Storer;

import org.eclipse.store.integrations.spring.boot.types.concurrent.Read;
import org.eclipse.store.integrations.spring.boot.types.concurrent.Write;
import org.eclipse.store.storage.embedded.types.EmbeddedStorageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.vertexprize.med.ecg.storage.model.MedicalDiagnosticsDatabase;
import org.vertexprize.medicine.ecg.data.OptionalContainer;
import org.vertexprize.medicine.ecg.medical.diagnostics.EDFrecord;

/**
 * Управление операций хранилища файлов формата EDF (EDF+)
 *
 * @author vaganovdv
 */
@Component
public class EDFrecordStorage implements EDFrecordStorageAPI {

    private final Logger log = LoggerFactory.getLogger(EDFrecordStorage.class);
    
    private final EmbeddedStorageManager storageManager;

    public EDFrecordStorage(EmbeddedStorageManager storageManager) {
        this.storageManager = storageManager;
        if (storageManager.root() == null) {
            log.info("Первоначальная инициализация базы данных ....");            
            storageManager.setRoot(new MedicalDiagnosticsDatabase());
            storageManager.storeRoot();
            log.info("Первоначальная инициализация базы данных ЗАВЕРШЕНА");
        }
    }

    /**
     * Реализация интерфеса чтения
     *
     * @param id
     * @return
     */
    @Read
    @Override
    public OptionalContainer<EDFrecord> readEdfFile(String id) {

        OptionalContainer<EDFrecord> c = new OptionalContainer<>();
        MedicalDiagnosticsDatabase database = (MedicalDiagnosticsDatabase) storageManager.root();
        Optional<EDFrecord> found = database.edfFiles().filter(file -> file.getId().equalsIgnoreCase(id)).findFirst();
        if (found.isPresent()) {
            c.setValue(found.get());
            c.setProcessed(true);
        }
        return c;
    }

    @Read
    @Override
    public OptionalContainer<List<EDFrecord>> readAllEdfFiles() {
        OptionalContainer<List<EDFrecord>> c = new OptionalContainer<>();
        
        MedicalDiagnosticsDatabase database = (MedicalDiagnosticsDatabase) storageManager.root();       
        List<EDFrecord> list = new ArrayList<EDFrecord>(database.edfFiles().collect(Collectors.toList()));         
        
        if (list != null && !list.isEmpty()) {
            log.debug("Прочитано ["+list.size()+"] записей базы медицинских обследований");            
            c.setValue(list);
            c.setProcessed(true);
        }
        return c;
    }

    @Write
    @Override
    public OptionalContainer<EDFrecord> writeEdfFile(EDFrecord file) {
        OptionalContainer<EDFrecord> c = new OptionalContainer<>();
        MedicalDiagnosticsDatabase database = (MedicalDiagnosticsDatabase) storageManager.root();
        database.addEdfFile(file);
        
        
        Storer eagerStorer = storageManager.createEagerStorer();
        eagerStorer.store(database);        
        eagerStorer.commit();
       
        c.setValue(file); 
        c.setProcessed(true);
        log.debug("Записан файл медицинских обследований:  "+file.getId());            
        
        return c;
    }

    @Write
    @Override
    public OptionalContainer<Long> writeAllEdfFiles(List<EDFrecord> fileList) {
        OptionalContainer<Long> c = new OptionalContainer<>();

        MedicalDiagnosticsDatabase database = (MedicalDiagnosticsDatabase) storageManager.root();
        
        Storer eagerStorer = storageManager.createEagerStorer();
        eagerStorer.store(database);
        eagerStorer.commit();
        
       
        c.setValue(database.edfFiles().count());        
        return c ;
    }

    @Override
    public OptionalContainer<EDFrecord> deleteEdfFiles(String id) {

        OptionalContainer<EDFrecord> c = new OptionalContainer<>();

        MedicalDiagnosticsDatabase database = (MedicalDiagnosticsDatabase) storageManager.root();
        Optional<EDFrecord> found = database.edfFiles().filter(file -> file.getId().equalsIgnoreCase(id)).findFirst();
        if (found.isPresent()) {
            log.info("Обнаружен файл для удаления id = " + id);
            log.info("Удаление файла из хранилища ...");
            boolean remove = database.getEDFFfiles().remove(found.get());
            log.info("Резульат удаления ==> " + remove);

            if (remove) {
                Storer eagerStorer = storageManager.createEagerStorer();
                eagerStorer.store(database);
                eagerStorer.commit();
                c.setValue(found.get());
                c.setProcessed(true);
            } else {
                String error = "Ошибка удаления файла с идентифкатором: [" + id+"]";
                c.getErrors().add(error);
                log.error(error);
            }
        } else {
            String error = "Не найден файл EDF файл с идентифкатором: ["+id+"]";
            c.getErrors().add(error);
            log.error(error);
        }
        return c;
    }

}
