/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.med.ecg.storage.initializer;

import org.eclipse.serializer.reference.Lazy;
import org.eclipse.serializer.reference.LazyReferenceManager;
import org.eclipse.store.integrations.spring.boot.types.initializers.StorageContextInitializer;


/**
 *
 * Внимание! файл конфигурации вызывает ошибку! - закоментирован 26.12.2024
 * 
 * @author vaganovdv
 */
//@Component
public class StorageInit implements StorageContextInitializer {
 
    @Override
    public void initialize()
    {
        LazyReferenceManager.set(LazyReferenceManager.New(
                Lazy.Checker(
                        1_000_000, // timeout of lazy access
                        0.75       // memory quota
                )));
    }
}
