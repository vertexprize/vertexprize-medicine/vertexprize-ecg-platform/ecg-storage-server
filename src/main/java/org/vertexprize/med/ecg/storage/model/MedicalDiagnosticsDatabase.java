/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.med.ecg.storage.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.eclipse.serializer.reference.Lazy;
import org.vertexprize.medicine.ecg.medical.diagnostics.EDFrecord;

/**
 * Корневой класс для предоставления списка EDF файлов
 *
 *
 * @author vaganovdv
 */
public class MedicalDiagnosticsDatabase {

    /**
     * Список EDF файлов
     */
    private Lazy<List<EDFrecord>> edfFiles;

    public MedicalDiagnosticsDatabase() {
        super();
    }

    public List<EDFrecord> getEDFFfiles() {
        return Lazy.get(this.edfFiles);
    }

    /**
     * // * Добавление EDF файла в базу данных
     *
     * @param file
     */
    public void addEdfFile(final EDFrecord file) {
        List<EDFrecord> files = this.getEDFFfiles();
        if (files == null) {
            this.edfFiles = Lazy.Reference(files = new ArrayList<>());
        }
        files.add(file);
    }

    
    /**
     * Чтение полного списка EDF файлов
     * 
     * @return 
     */
    public Stream<EDFrecord> edfFiles() {
        final List<EDFrecord> files = this.getEDFFfiles();
        return files != null ? files.stream() : Stream.empty();
    }

}
