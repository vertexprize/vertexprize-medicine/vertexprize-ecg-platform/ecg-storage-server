package org.vertexprize.med.ecg.storage;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.vertexprize.med.ecg.storage.api.EDFrecordStorage;
import org.vertexprize.medicine.ecg.data.OptionalContainer;
import org.vertexprize.medicine.ecg.medical.diagnostics.EDFheader;
import org.vertexprize.medicine.ecg.medical.diagnostics.EDFrecord;

@SpringBootApplication
public class StorageServerApplication {

    private final Logger log = LoggerFactory.getLogger(StorageServerApplication.class);
    private int index = 0;

    
    // Класс для организации хранилища EDF файлов 
    @Autowired
    private EDFrecordStorage storage;

    public static void main(String[] args) {
        SpringApplication.run(StorageServerApplication.class, args);
    }

    /**
     * Метод, запускаемый при старте сервера 
     */
    @PostConstruct
    public void start() {
        log.info("Старт сервера: [" + StorageServerApplication.class.getSimpleName() + "]...");
        init();
        log.info("Сервер: [" + StorageServerApplication.class.getSimpleName() + "] ЗАПУЩЕН");
    }

    @PreDestroy
    public void stop() {
        log.info("Остановка сервера: [" + StorageServerApplication.class.getSimpleName() + "]...");

        OptionalContainer<List<EDFrecord>> files = storage.readAllEdfFiles();
        if (files.isProcessed()) {
            log.info("База данных содежит [" + files.getValue().size() + "] записей медицинских исследований");
        } else {
            log.info("База данных медицинских исследований ПУСТАЯ");
        }

        log.info("Сервер: [" + StorageServerApplication.class.getSimpleName() + "] ОСТАНОВЛЕН");
    }

    
    
    /**
     * Формирование тестовых данных
     * 
     */
    private void init() {
        OptionalContainer<List<EDFrecord>> files = storage.readAllEdfFiles();
        if (files.isProcessed()) {
            log.info("База данных содежит [" + files.getValue().size() + "] записей медицинских исследований");

            index = 0;
            StringBuilder sb = new StringBuilder();
            sb.append("\n");
            files.getValue().stream().forEach(f -> {
                index++;
                sb.append(String.format("%-10s", "[" + index + "]"));
                sb.append(f.toString());
                sb.append("\n");
            });
            log.info(sb.toString());

        } else {
            log.info("База данных медицинских исследований ПУСТАЯ");
        }

        // Формирование тестовых данных
        List<EDFrecord> list = new ArrayList<>();

//        
//        EDFrecord r1 = new EDFrecord();
//        EDFrecord r2 = new EDFrecord();
//        EDFrecord r3 = new EDFrecord();
//        
//        EDFheader h1 = new EDFheader();
//        h1.setPatientName("Петрова Лилия Петровна");
//        h1.setStartDate(LocalDate.now());
//        h1.setStartTime(LocalTime.now());
//        h1.setGender("женский");
//        h1.setNumberOfSamples(100);
//        
//        EDFheader h2 = new EDFheader();
//        h2.setPatientName("Иванов Иван Иванович");
//        h2.setStartDate(LocalDate.now());
//        h2.setStartTime(LocalTime.now());
//        h2.setGender("мужской");
//        h2.setNumberOfSamples(20);
//        
//        r1.setEdfHeader(h1);
//        r2.setEdfHeader(h1);
//        r3.setEdfHeader(h2);
//        
//        r1.setPatientId("422b008c-2830-42f5-a81d-d39971c37dbe");
//        r2.setPatientId("422b008c-2830-42f5-a81d-d39971c37dbe");
//        r3.setPatientId("7cb81dde-09b2-4667-9a30-0190e8e09dc7");
//        
//        r1.setDescription("ЭКГ-1");
//        r2.setDescription("ЭКГ-2");
//        r3.setDescription("ЭЭГ-1");
//        
//        r1.setType("Электрокардиограмма");
//        r2.setType("Электрокардиограмма");
//        r3.setType("Электроэнцефалография");
//        
//        
//        list.add(r1);
//        list.add(r2);
//        list.add(r3);
//        
//        
//
//        for (EDFrecord file : list) {
//            OptionalContainer<EDFrecord> writeEdfFile = storage.writeEdfFile(file);
//            if (writeEdfFile.isProcessed()) {
//                log.info("Записано в базу: " + writeEdfFile.getValue().toString());
//            }
//        }

    }

}
