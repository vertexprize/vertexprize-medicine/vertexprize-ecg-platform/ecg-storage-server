/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package org.vertexprize.med.ecg.storage.api;

import java.util.List;
import org.vertexprize.medicine.ecg.data.OptionalContainer;
import org.vertexprize.medicine.ecg.medical.diagnostics.EDFrecord;

/**
 * Интерфейс для опреления действий с хранилищем EDF файлов
 *
 * @author vaganovdv
 */
public interface EDFrecordStorageAPI {

    /**
     * Загрузка единичного файла EDF
     *
     * @param id  уникальный идентификатор обследования
     * @return 
     */
    OptionalContainer<EDFrecord> readEdfFile(String id);

    /**
     * Загрузка полного списка файлов EDF
     *
     * @return
     */
    OptionalContainer<List<EDFrecord>> readAllEdfFiles();
    
    /**
     * Сохранение в хранилище единичного файла EDF
     * @param file
     * @return 
     */
    OptionalContainer<EDFrecord>  writeEdfFile (EDFrecord file);
    
    
    /**
     * Сохранение списка EDF файлов  в хранилище 
     * @param file
     * @return 
     */
    OptionalContainer<Long> writeAllEdfFiles (List<EDFrecord> file);
    
    
    /**
     * Удаление единичного файла EDF из хранилища
     * 
     * @param id
     * @return 
     */
    OptionalContainer<EDFrecord> deleteEdfFiles (String id);
    

}
