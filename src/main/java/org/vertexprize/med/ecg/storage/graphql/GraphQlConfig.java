/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.med.ecg.storage.graphql;

import graphql.scalars.ExtendedScalars;
import graphql.schema.GraphQLScalarType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.graphql.execution.RuntimeWiringConfigurer;



/**
 *
 * @author vaganovdv
 */
@Configuration
public class GraphQlConfig {

    
    @Bean
    public RuntimeWiringConfigurer runtimeWiringConfigurer() {
        GraphQLScalarType scalarType = ExtendedScalars.GraphQLLong ;
       // SchemaDirectiveWiring directiveWiring = ... ;
        //DataFetcher dataFetcher = QuerydslDataFetcher.builder(repository).single();

        return wiringBuilder -> wiringBuilder
                .scalar(ExtendedScalars.GraphQLLong)
                .scalar(ExtendedScalars.Date)  
                .scalar(ExtendedScalars.Json) 
                .scalar(ExtendedScalars.Time) 
                .scalar(ExtendedScalars.LocalTime)                 
                .scalar(ExtendedScalars.PositiveInt) 
                .build();
                
               
                //.directiveWiring(directiveWiring)
                //.type("Query", builder -> builder.dataFetcher("book", dataFetcher));
    }
}
