/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.med.ecg.storage.graphql;

import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.DgsQuery;
import com.netflix.graphql.dgs.InputArgument;
import graphql.schema.DataFetchingEnvironment;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertexprize.med.ecg.storage.api.EDFrecordStorage;
import org.vertexprize.medicine.ecg.data.OptionalContainer;
import org.vertexprize.medicine.ecg.medical.diagnostics.EDFrecord;

/**
 *
 * @author vaganovdv
 */
@DgsComponent
public class EDFfileDataFetcher {

    private final Logger log = LoggerFactory.getLogger(EDFfileDataFetcher.class);

    private final EDFrecordStorage storage;

    public EDFfileDataFetcher(EDFrecordStorage storage) {
        this.storage = storage;
    }

    /**
     * Тестовая (учебная база данных) записей медицинской диагностики
     *
     * //
     */
//  private final List<EDFfile> files = List.of(
//          new EDFfile("faa27343-71d7-49fb-8b00-562639caa60d", "Обследование 1", "Кадиограмма (ЭКГ)", UUID.randomUUID().toString()),
//          new EDFfile("473b76cc-b2e6-4078-91bb-5085e26be15d", "Обследование 2", "Кадиограмма (ЭКГ)", UUID.randomUUID().toString()),
//          new EDFfile("8083ea43-6218-4461-a0f4-ae6511f2ac3a", "Обследование 3", "Электроэнцефалография", UUID.randomUUID().toString()),
//          new EDFfile("881e7f6a-a7fb-412a-abc5-2e99dddf3957", "Обследование 4", "Электроэнцефалография", UUID.randomUUID().toString()),
//          new EDFfile("a3c155bf-3a8b-429b-8284-d88a81c072db", "Обследование 5", "Электроэнцефалография", UUID.randomUUID().toString())
//  );
    /**
     * Запрос полного списка файлов
     *
     * Метод, реализующий обработку запроса GraphQL c именем
     * edfFilesByDiagnosticType (см. файл schema.graphqls)
     *
     * @param type
     * @return
     */
    @DgsQuery
    public List<EDFrecord> edfFilesByDiagnosticType(@InputArgument String type) {
        List<EDFrecord> list = new ArrayList<>();

        log.info("Выполнение запроса получения всех файлов edf ...");
        
        if (type == null) {
            // Чтение всех файлов, если не указан тип
            OptionalContainer<List<EDFrecord>> result = storage.readAllEdfFiles();
            if (result.isProcessed()) {
                list.addAll(result.getValue());
            }
        } else {
            // Формирование списка файлов с типом type
            OptionalContainer<List<EDFrecord>> result = storage.readAllEdfFiles();
            if (result.isProcessed()) {
                list = result.getValue().stream().filter(s -> s.getType().contains(type)).collect(Collectors.toList());
            }
        }
        return list;
    }

    /**
     * Сохранение в хранилище единичного файла EDF
     *
     * @param dataFetchingEnvironment
     * @return
     */
    @DgsData(parentType = "Mutation", field = "writeEdfFile")
    public EDFrecord writeEdfFile(DataFetchingEnvironment dataFetchingEnvironment) {

        String description = dataFetchingEnvironment.getArgument("description");
        String type = dataFetchingEnvironment.getArgument("type");
        String patientId = dataFetchingEnvironment.getArgument("patientId");

        EDFrecord file = new EDFrecord(description, type, patientId);

        if (description == null || description.isEmpty()) {
            throw new IllegalArgumentException("Описание обследование не может быть пустое");
        }
        if (type == null || type.isEmpty()) {
            throw new IllegalArgumentException("Тип обследования не может быть пустым");
        }
        if (patientId == null || patientId.isEmpty()) {
            throw new IllegalArgumentException("Идентификатор пациента не может быть пустым");
        }

        OptionalContainer<EDFrecord> result = storage.writeEdfFile(file);
        if (result.isProcessed()) {
            log.info("Сохранено ==> " + file.toString());
        } else {
            file = null;
            log.error("Ошибка сохранения файла EDF");
        }
        return file;
    }

    
    /**
     * Удаление файла EDF из хранилища
     * 
     * @param dataFetchingEnvironment
     * @return 
     */
    @DgsData(parentType = "Mutation", field = "deleteEdfFiles")
    public EDFrecord deleteEdfFiles(DataFetchingEnvironment dataFetchingEnvironment) {

        EDFrecord file = null;

        String id = dataFetchingEnvironment.getArgument("id");

        OptionalContainer<EDFrecord> result = storage.deleteEdfFiles(id);
        if (result.isProcessed()) {
            file = result.getValue();
            log.info("Удалено ==> " + result.getValue().getId());
        } else {
            file = null;
            log.error("Ошибка удаления файла EDF");
        }
        return file;
    }
    
}
