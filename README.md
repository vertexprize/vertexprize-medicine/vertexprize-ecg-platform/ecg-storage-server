# ecg-storage-server

Сервер для организации хранения файлов медицинских исследований.

## Требования к среде выполнения и разработки 

Для разработки и выполнения кода приложения требуется:

- Java версии не ниже 21 (выполнение кода и разработка);
- maven версии не ниже 3.9.9 (работка и сборка).

## Назначение

Сервер предназначен для организации хранения файлов в формате EDF и EDF+
в хранилище EclipseStore.  Доступ к хранилищу файлов организован с использованием протокола GraphQL

## Используемые технологии

- [ ] [Проект хранилища Eclipse Store](https://eclipsestore.io/)
- [ ] [Spring framework](https://spring.io/projects/spring-framework)
- [ ] [Фреймворк DGS компании Netflix для разработки сервисов с использование  протокола GraphQL](https://netflix.github.io/dgs/)

## Примечания к выпускам

`0.0.18`  Переход на версию Graphql  с подержкой Spring GraphQL (в разработке)
`0.0.17`  Версия, разработанная на основе фреймворка  DGS.  Поддержка форматов EDF реализована частично (рабочий код)

## Клонирование проекта

Для клонирования проекта выполнить команду:

```bash
git clone https://gitlab.com/vertexprize/vertexprize-medicine/vertexprize-ecg-platform/ecg-storage-server.git
```

## Настройка проекта 

Настройка проекта выполняется в 2 этапа:

1. Создание каталога для организации хранилища Eclipse Store и конфурация приложения

В файловой системе необходимо создать каталог для хранения файлов Eclipse Store c использованием следующей команды: 

```bash
mkdir datastore
```

В рассматриваемом случае в текущем каталоге создан каталог `datastore`.

Предположим, что полный путь к хранилищу Eclipse Store это каталог `/home/developer/datastore`. 
Необходимо настроть приложение `ecg-storage-server` для того, чтобы файлы хранилища были организованы в каталоге  `/home/developer/datastore`.
Для выполнения настройки необходимо отредактировать файл конфигурации `application.yaml` приложения SpringBoot. 
Файл конфигурации размещен в каталоге `ecg-storage-server/src/main/resources`.
Для указания местоположения файлов необходимо отредактировать параметр `storage-directory` раздела `org.eclipse.store` - указать путь к файлам org.eclipse.store:
`/home/developer/datastore` как показано в следующем фрагменте:

```yaml
spring:
  lifecycle:
    timeout-per-shutdown-phase: 30s
  application:
    name: ECG-storage-server
     

###  Конфигурация Spring boot 
server:  
  port : 7779
  
# Настройка Eclipse Store  
org.eclipse.store:
  # Местоположение баз данных eclipse store
  storage-directory: /home/developer/datastore
  channel-count: 1

  
# Настройка лога 
logging:
  level:
    root: INFO
    org.springframework.web: INFO
    org.vertexprize.med.ecg.storage: INFO
    org.hibernate: OFF
```

2. Конфигурирование  каталога для размещения логов

В файловой системе необходимо создать каталог для хранения файлов логов приложения Spring Boot c использованием следующей команды: 

```bash
mkdir logs
```

Для выполнения настройки необходимо отредактировать файл конфигурации `logback.xml` приложения SpringBoot.
В рассматриваемом случае в текущем каталоге создан каталог `logs`.
Необходимо настроть приложение `ecg-storage-server` для того, чтобы файлы логов приложения SpringBoot были организованы в каталоге  `/home/developer/logs`.
Для выполнения настройки необходимо отредактировать файл конфигурации `logback.xml` приложения SpringBoot.Файл конфигурации размещен в каталоге `ecg-storage-server/src/main/resources`.

Для указания местоположения файлов необходимо отредактировать параметр `file` раздела `appender name="FILE" class="ch.qos.logback.core.rolling.RollingFileAppender"` - указать путь к файлам логов `/home/developer/logs` как показано в следующем фрагменте:

```xml

<?xml version="1.0" encoding="UTF-8"?>
<configuration>
 
    <appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
        <layout class="ch.qos.logback.classic.PatternLayout">
            <Pattern>
                %d{HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n
            </Pattern>
        </layout>
    </appender>

    <logger name="org.eclipse.store" level="debug" additivity="false">
        <appender-ref ref="CONSOLE"/>
    </logger>
 

   <appender name="FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
    <file>/home/developer/logs/storage-server.log</file>
    <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
        <!-- daily rollover -->
        <fileNamePattern>${LOG_FILE}.%d{yyyy-MM-dd}.gz</fileNamePattern>
        <!-- keep 30 days' worth of history capped at 3GB total size -->
        <maxHistory>30</maxHistory>
        <totalSizeCap>20MB</totalSizeCap>
    </rollingPolicy>
    <encoder>
        <pattern>%-4relative [%thread] %-5level %logger{35} - %msg%n</pattern>
    </encoder>
  </appender> 
    
    <root level="error">
        <appender-ref ref="CONSOLE"/>
        <appender-ref ref="FILE" />
    </root>
    
    <root level="info">
        <appender-ref ref="CONSOLE"/>
        <appender-ref ref="FILE" />
    </root>

    <root level="warn">
        <appender-ref ref="CONSOLE"/>
        <appender-ref ref="FILE" />
    </root>

</configuration>
```

## Запуск приложения   `ecg-storage-server`

Для запуска приложения `ecg-storage-server` необходимо перейти в каталог, содержащий клонированнй проект с использованием команды:

```bash
cd ecg-storage-server
```

Далее необходимо выполнить команду запуска приложения:

```bash
    ./mvnw spring-boot:run
```
